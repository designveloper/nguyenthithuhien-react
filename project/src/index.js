import React from 'react'
import { render } from 'react-dom'
import { SkiDayCount } from './Components/SkiDayCount-createClass'
import { SkiDayList } from './Components/SkiDayList'
import { App } from './Components/App'
import { Router, Route, hashHistory} from 'react-router'
import { Whoops404 } from './Components/Whoops404'
import {AddDayForm}  from './Components/AddDayForm'
import './stylesheets/ui.scss'
import './stylesheets/index.scss'

Window.React = React;

// render(
// 	<SkiDayCount />,
// 	document.getElementById('react-container')
// )

// render(
// 	<SkiDayList days={
// 		[
// 			{
// 				resort: "Resort A",
// 				date: new Date("1/2/2016"),
// 				powder: true,
// 				backcountry: false
// 			},
// 			{
// 				resort: "Resort B",
// 				date: new Date("3/7/2016"),
// 				powder: false,
// 				backcountry: false
// 			},
// 			{
// 				resort: "Resort C",
// 				date: new Date("4/29/2016"),
// 				powder: false,
// 				backcountry: true
// 			}
// 		]
// 	} />,
// 	document.getElementById('react-container')
// )

render(
	<Router history = {hashHistory}>
		<Route path="/" component={App} />
		<Route path="/list-days" component={App} >
			<Route path=":filter" component = {App}/>
		</Route>
		<Route path="/add-day" component={App} />
		<Route path="*" component={Whoops404} />
	</Router>,
	document.getElementById('react-container')
)
