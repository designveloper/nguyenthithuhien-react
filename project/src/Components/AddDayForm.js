import React , {PropTypes, Component} from 'react'
import '../stylesheets/ui.scss'

const Resorts = [
  "Adsjfh",
  "Bdhs",
  "Cuy",
  "Dek",
  "Ekejf"
]

class Autocomplete  extends Component {
  get value(){
    return this.refs.inputResort.value
  }

  set value(inputValue) {
    this.refs.inputResort.value = inputValue
  }

  render(){
    return (
      <div>
        <input ref="inputResort" type="text" list="list-resorts" />
        <datalist id="list-resorts">
          {this.props.options.map(
            (opt, i) => <option key={i}>{opt}</option>
          )}
        </datalist>
      </div>
    )
  }
}

export const AddDayForm = ({resort, date, powder, backcountry, onNewDay}) => {

  let _resort, _date, _powder, _backcountry

  const submit = (e) => {
    e.preventDefault()
    onNewDay({
      resort: _resort.value,
      date: _date.value,
      powder: _powder.checked,
      backcountry: _backcountry.checked,

    })
    _resort.value = ''
    _date.value = ''
    _powder.checked = false
    _backcountry.checked = false
  }

  return (
    <form className="add-day-form" onSubmit={submit}>
      <div>
        <label htmlFor="resort">Resort Name</label>
        <Autocomplete options={Resorts} ref={input => _resort = input}/>
      </div>
      <div>
        <label htmlFor="date">Date</label>
        <input id="date" type="date" require defaultValue={date} ref={input => _date = input}/>
      </div>
      <div>
        <input id="powder" type="checkbox"  defaultChecked ={powder} ref={input => _powder = input}/>
        <label htmlFor="powder">Powder Day</label>
      </div>
      <div>
        <input id="backcountry" type="checkbox" defaultChecked ={backcountry} ref={input => _backcountry = input}/>
        <label htmlFor="backcountry">Backcountry Day</label>
      </div>
      <button>Add Day</button>
    </form>
  )
}


AddDayForm.defaultProps = {
  resort: "Resort ABC",
  date: "2017-08-10",
  powder: true,
  backcountry: false
}
AddDayForm.propTypes = {
  resort: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  powder: PropTypes.bool.isRequired,
  backcountry: PropTypes.bool.isRequired,
}
