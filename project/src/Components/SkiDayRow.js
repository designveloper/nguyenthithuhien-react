import React , {PropTypes} from 'react'
import Terrain from 'react-icons/lib/md/terrain'
import SnowFlake from 'react-icons/lib/ti/weather-snow'
import Calendar from 'react-icons/lib/fa/calendar'
import '../stylesheets/ui.scss'
import '../stylesheets/index.scss'


export const SkiDayRow = ({resort, date, powder, backcountry}) => (
  <tr>
    <td>
      {date}
    </td>
    <td>
      {resort}
    </td>
    <td>
      {(powder) ? <SnowFlake /> : null }
    </td>
    <td>
      { (backcountry) ? <Terrain /> : null}
    </td>
  </tr>
)

SkiDayRow.propTypes = {
  date: PropTypes.string.isRequired,
  resort: PropTypes.string.isRequired,
  powder: PropTypes.bool,
  backcountry: PropTypes.bool,
}
