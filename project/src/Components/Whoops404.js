import React from 'react'
import '../stylesheets/ui.scss'
import '../stylesheets/index.scss'

export const Whoops404 = () =>
  <div>
    <h1>Whoops, route not found</h1>
  </div>
