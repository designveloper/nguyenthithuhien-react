/**************** REACT *****************/
## Run my project
- `npm install`
- `npm start`

React is a JS library that's used to build user interfaces.

## 1. What is React.js?
- What is React?
	+ A library for user interfaces
	+ Created at Facebook and Instagram
	+ React Native for mobile
- Setting up Chrome tools for React:
	+ Add extension react-detector
	+ And React Developer Tools: describe all of the React elements that make up a page.
- Efficient rendering with React:
	+ The DOM (Document Object Model): is the structure of HTML elements.
	+ The DOM API refers to how these page elements are accessed and changed.
	+ DOM Diffing:
		- Compares rendered content with the new UI changes
		- Makes only the minimal changes necessary
		- Compares JS objects
		- IS faster than writing to or reading from DOM
	+ Updating the DOM is fast in React because React makes the minimal changes to make an update.

## 2. Intro to JSX and Babel:
- Pure React
	+ Create Element => Render Element
		* Create Element: createElement('nameOfElement', {id: '', className: '', style: {} }, 'contentOfElement');
		* Render: render(Element, Position)
	+ Start up the server to load these files easily.
		- npm install -g httpster (administrator)
		- Then, Can run the server local on my computer.
- Refactoring elements using JSX
	+ JSX is javascript as XML
	+ JSX isn't supported in browser.
- Babel inline transpiling
	+ JSX isn't supported in browser. So I want to use it, I have to transpile it, use Babel-core.
	+ Go to page: http://babeljs.io/ to transpile online JSX.
	+ Or you can: find link CND to transpile: https://cdnjs.com/libraries/babel-core
- Babel static transpiling with babel-cli
	+ Install babel-cli:
		- I have to generator project with 'npm init' command before.
		- npm install babel-cli --save-dev
	+ All file React code JSX in one folder.
	+ Create file : .babelrc : set up all the presets or everything we want to transpile using Babel.
		{
			presets : ['latest', 'react', 'stage-0']
		}
	+ Install preset:
		npm install --save-dev babel-preset-latest babel-preset-react babel-preset-stage-0
	+ Transpiling with babel
		babel <url_file_to_tranpile> --out-file <url_file_tranpiled>

- Building with webpack
	+ Webpack:
		- Is a module bundler
		- Creates static files
		- Automates processes
	+ Create `webpack.config.js` file to set up webpack
	+ Install `webpack` package --save-dev and -global
	+ Install `babel-loader` package
	+ Install `webpack-dev-server` package

- Loading JSON with webpack
	+ Install react and react-dom
	+ Import `react`, import file data JSON
	+ Install `json-loader` package
	+ Edit file webpack.config.js: add to loader
- Adding CSS to webpack build
	+ Create file "abc.scss", I can use Webpack to transpile to .css
	+ In file "abc.scss":
		* Declare variables: $nameVar : value;
		* Then, set up style through variables are declared same as CSS
	+ Install style-loader, css-loader, autoprefixer-loader, sass-loader, node-sass
	+ Edit file webpack.config.js: add to loader

## 3. React Components
### Planning an ActivityCounter
### Creating components with createClass()
- To create Components, use React.createClass();
- export const nameOfComponent = React.createClass()
### Adding components properties
- When I call Component, I can add properties to it. To use properties: this.props.nameOfProps

### Adding component methods
### Creating components with ES6 class syntax
- export class nameOfComponent extends React.Component {}
- Remove comma between functions
- I can `import {Component} from 'react'` and use "extends Component"

### Creating stateless functional components
- const nameOfComponent = (props) => (
	<div>{props.title}</div>
	)
- Return JSX elements
### Adding react-icons
- Install react-icons from NPM before I use
- Link:https://gorangajic.github.io/react-icons/fa.html
- Import icons, and use it same as component
- Ex: <NameOfIcon />

## 4. Props and state
### Composing components
- Create a new component
### Displaying child components
- Use ArrayObject.map(callback arrow function): same as loop in array
- Use map(callback arrow function): same as loop
- When I render children from an array, I need to supply `key` value and props necessary
### Default props
- With Component from createClass: add function `getDefaultProps` to Component
- With Component ES6 class:  add `nameOfComponent.defaultProps  = {prop: value, ...}` to outside the component
- With stateless functional component: set defaultProps same as ES6 class. Moreover, I can set default props directly to props.
### Validating with React.PropTypes
- PropTypes allow us to supply a property type for all properties.
- PropTypes are a feature
- Ref: https://facebook.github.io/react/docs/typechecking-with-proptypes.html
- Import PropTypes before using
- With createClass component: add `propTypes : { prop1: PropTypes.number.isRequired,...}` to Component
- With ES6 Component: add `nameOfComponent.propTypes = {prop1: PropTypes.number.isRequired,...}` to outside the component
- With stateless functional component: same as ES6 component

### Custom validation
### Working with state
- State represents the possible conditions of your application.
- State for editing, saved or logged in and logged out
- Identify the minimal representation of app state.
- Reduce state to as few components as possible
- Avoid overwriting state variables
- With createClass component: add `getInitialState(){ return { state1: value1, state2: value2,..} }` to the component.

### Passing state as props
### State with ES6 classes
- Instead of getInitialState{}, using
	constructor(props) {
		super(props)
		this.state= {state1: val1, state2: val2}
  }
- I can't set up initial state in a stateless functional component

## 5. Using the React Router
### Incorporating the router
- Install `react-router` package:
- import {Router, Route, hashHistory} from 'react-router'
- Add history = {hashHistory} to <Router>, history listen to the browser's address bar for any changes and it will keep track of those changes.

### Setting up routes
### Navigating with the link component
- import {Link} from 'react-router'
- this.props.location.pathname: to get current path name
- <Link to 'path' />

### Using route parameters
- Route with params
- <Route path="/items" component={nameOfComponent}> <Route path=":filter" component={nameOfComponent}/> </Route>
- To get that params: `this.props.params.filter`

### Nesting routes

## 6. Forms and Refs
### Creating a form component
### Using refs in class components
- I want to reach out to individual elements to figure out what their values are, use `refs`
- Add each input property: ref ="nameOfRef"
- To get value of input: this.refs.nameOfRef.value

### Using refs in stateless components
- In Stateless Component there is no `this`
- Add each input property: `ref= {input => nameofRef = input}`
- Need to declare `nameofRef` before using
- To get value of input: `nameofRef.value`
- Store value of input to variables locally

### Two-way function binding
- this.nameOfFunc = this.nameOfFunc.bind(this)

### Adding an autocomplete component
- Use datalist

## 7. The Component Lifecycle
- Each component has several "Lifecycle methods" that I can override to run code at particular times in the process.
- Methods prefixed with `will` are called right before sth happens
- Methods prefixed with `did` are called right after sth happens
- Ref: https://facebook.github.io/react/docs/react-component.html

### Understanding the mounting Lifecycle
- These methods are called when an instance of a component is being created and inserted into the DOM:
	+ constructor()
		+ The constructor for a React component is called before it is mounted.
		+ When implementing the constructor, you should call `super(props)` before any other statement. Otherwise, `this.props` will be undefined in the constructor, which can lead to bugs.
		+ The constructor is the right place to initialize state and have to bind methods, you can implement a constructor for React component.
	+ componentWillMount()
		+ is invoked immediately before mounting occurs.
		+ It is called before render(), therefore setting state synchronously in this method will not trigger a re-rendering.
		+ Avoid introducing any side-effects or subscriptions in this method.
	+ render()
		+
	+ componentDidMount
		+ is invoked immediately after a component is mounted
		+ Initialization that requires DOM nodes should go here.
		+ If you need to load data from a remote endpoint, this is a good place to instantiate the network request.
		+ Setting state in this method will trigger a re-rendering.

### Understanding the updating Lifecycle
- An update can be caused by changes to props or state. These methods are called when a component is being re-rendered:
	+ componentWillReceiveProps()
		+ If you need to update the state in response to prop changes, you may compare this.props and nextProps and perform state transitions using this.setState() in this method.
	+ shouldComponentUpdate()
	+ componentWillUpdate()
	+ render()
	+ componentDidUpdate()
